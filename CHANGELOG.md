# 版本日志
---------------------------------------------------------------------

## 1.2.3
*  【common-boot】 拦截器包不扫描抽象类
*  添加SpringBoot 配置文档  让配置属性不再困难

---------------------------------------------------------------------

## 1.2.2
*  【common-boot】 新增[request.log]配置是否记录请求日志信息和响应超时信息
*  【common-boot】 新增[request.parameterXss]配置是否强制过滤xss标签问题
*  【common-boot】 全局过滤器修护ISO-8859-1 转UTF-8 问题
*  【common-boot】 新增扫描拦截器包一下的 HandlerMethodArgumentResolver 实现类，来添加参数解析器

---------------------------------------------------------------------

## 1.2.1
*  【common-boot】 拦截注解添加排序属性  排序值越小越先加载
*  【common-redis】 去掉读取配置文件，采用动态读取属性 
*  【common-boot】 删除simple-util 依赖
*  【common-boot】 xss自动支持文件表单
*  【去除】AbstractMultipartFileBaseControl 类 合并到 AbstractBaseControl

---------------------------------------------------------------------

## 1.2.0
*  去掉默认依赖SpringBoot parent  只需要配置  spring-boot.version 来指定版本 
*  引入hutool 工具集
*  去掉原PagekageUtil

---------------------------------------------------------------------

## 1.1.23
*  【common-boot】            添加EnableCommonBoot注解来注入程序

---------------------------------------------------------------------